import dotenv from 'dotenv';
import express, {Express, Request, Response} from 'express';
import { getTimeStamp } from './utilits';


dotenv.config();

const app: Express = express();
const port = process.env.PORT;

app.get('/',(req: Request, res: Response) =>{
    res.send(`Express server for homework. Timestamp = ` + getTimeStamp())
})

app.listen(port,() => {
    console.log(`Server is running at http://localhost:${port}`)
})

export default app;
