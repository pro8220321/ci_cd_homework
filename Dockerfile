FROM node:18-alpine
WORKDIR  /usr/app
COPY ./ /usr/app
RUN npm install
RUN npm build
EXPOSE 8001
CMD ["npm","start"]